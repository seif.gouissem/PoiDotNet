using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Isen.DotNet.Library.Models.Implementation;
using Isen.DotNet.Library.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Isen.DotNet.Library.Data
{
    public class SeedData
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<SeedData> _logger;
        private readonly IPoiRepository _poiRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly ITownRepository _townRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IDepartementRepository _departementRepository;

        public SeedData(
            ApplicationDbContext context,
            ILogger<SeedData> logger,
            ITownRepository townRepository,
            IPoiRepository poiRepository,
            IAddressRepository addressRepository,
            ICategoryRepository categoryRepository,
            IDepartementRepository departementRepository)
        {
            _context = context;
            _logger = logger;
            _poiRepository = poiRepository;
            _addressRepository = addressRepository;
            _townRepository = townRepository;
            _categoryRepository = categoryRepository;
            _departementRepository = departementRepository;
        }

        public void DropDatabase()
        {
            var deleted = _context.Database.EnsureDeleted();
            var not = deleted ? "" : "not ";
            _logger.LogWarning($"Database was {not}dropped.");
        }

        public void CreateDatabase()
        {
            var created = _context.Database.EnsureCreated();
            var not = created ? "" : "not ";
            _logger.LogWarning($"Database was {not}created.");
        }

        public void AddDepartements()
        {

            


            if (_departementRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding departements");

            string json = File.ReadAllText(@"..\Isen.DotNet.Library\data\departements.json");
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);

            DataTable dataTable = dataSet.Tables["Departement"];

            var departements = new List<Departement>();
            
                foreach (DataRow row in dataTable.Rows)
                {
                departements.Add(new Departement { Name = (string)row["nom"], Code = (string) row["code"] });
                }

                _departementRepository.UpdateRange(departements);
                _departementRepository.Save();

                _logger.LogWarning("Added departements");
            
            
            
            /*
            if (_departementRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding departements");

            var departements = new List<Departement>
            {
                new Departement { Name = "Var" , Code="83" }
            };
            _departementRepository.UpdateRange(departements);
            _departementRepository.Save();

            _logger.LogWarning("Added departements"); */
        }

        public void AddTowns()
        {
            if (_townRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding towns");

            string json = File.ReadAllText(@"..\Isen.DotNet.Library\data\communes.json");
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);

            DataTable dataTable = dataSet.Tables["Communes"];

            var towns = new List<Town>();

            foreach (DataRow row in dataTable.Rows)
            {
                towns.Add(new Town
                {
                    Name = (string)row["nom"],
                    Departement = _departementRepository.Single( (string) row["departement__nom"] ),
                    Longitude = (double)row["centre__coordinates__002"],
                    Latitude = (double)row["centre__coordinates__001"]
                });
            }

            _townRepository.UpdateRange(towns);
            _townRepository.Save();

            _logger.LogWarning("Added towns");
            /* if (_townRepository.GetAll().Any()) return;
             _logger.LogWarning("Adding towns");

             var towns = new List<Town>
             {
                 new Town { Name = "Toulon" , Latitude = 83.5978 , Longitude = 56.6578, Departement = _departementRepository.Single("Var") }
             };
             _townRepository.UpdateRange(towns);
             _townRepository.Save();

             _logger.LogWarning("Added towns");*/
        }

        public void AddCategories()
        {
            if (_categoryRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding categories");

            string json = File.ReadAllText(@"..\Isen.DotNet.Library\data\categories.json");
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);

            DataTable dataTable = dataSet.Tables["Categories"];

            var categories = new List<Category>();

            foreach (DataRow row in dataTable.Rows)
            {
                categories.Add(new Category
                {
                    Name = (string)row["nom"],
                    Description = (string)row["description"]
                });
            }

            _categoryRepository.UpdateRange(categories);
            _categoryRepository.Save();

            _logger.LogWarning("Added categories");

            /*if (_categoryRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding categories");

            var categories = new List<Category>
            {
                new Category { Name = "Restaurant" , Description = "L� o� on mange"}
            };
            _categoryRepository.UpdateRange(categories);
            _categoryRepository.Save();

            _logger.LogWarning("Added categories");*/
        }

        public void AddAddresses()
        {
           /* if (_addressRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding addresses");

            var addresses = new List<Address>
            {
                new Address {
                    TextLine = "10 Avenue du Mar�chal Juin" ,
                    ZipCode = "83000",
                    Town=_townRepository.Single("Toulon"),
                    Latitude = 83.5978,
                    Longitude = 56.6578
                }
            };
            _addressRepository.UpdateRange(addresses);
            _addressRepository.Save();

            _logger.LogWarning("Added addresses");*/
        }

        public void AddPois()
        {
            if (_poiRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding pois");

            string json = File.ReadAllText(@"..\Isen.DotNet.Library\data\pois.json");
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);

            DataTable dataTable = dataSet.Tables["Pois"];

            var pois = new List<Poi>();
            var addresses = new List<Address>();

            foreach (DataRow row in dataTable.Rows)
            {
                addresses.Add(new Address
                {
                    Name = (string)row["adresse__text"],
                    TextLine = (string)row["adresse__text"],
                    ZipCode = (string)row["adresse__code"],
                    Town = _townRepository.Single( (string)row["adresse__commune"] ),
                    Latitude = (double)row["adresse__lat"],
                    Longitude = (double)row["adresse__long"]
                });
            }

            _addressRepository.UpdateRange(addresses);
            _addressRepository.Save();

            _logger.LogWarning("Added addresses");

            foreach (DataRow row in dataTable.Rows)
            {
                pois.Add(new Poi
                {
                    Name = (string)row["nom"],
                    Description = (string)row["description"],
                    Address = _addressRepository.Single((string)row["adresse__text"]),
                    Category = _categoryRepository.Single((string)row["categorie__nom"])
                });
            }

            _poiRepository.UpdateRange(pois);
            _poiRepository.Save();

            _logger.LogWarning("Added pois");

            /* if (_poiRepository.GetAll().Any()) return;
             _logger.LogWarning("Adding pois");

             var pois = new List<Poi>
             {
                 new Poi {
                     Name = "McDonalds Avenue Juin",
                     Address=_addressRepository.Single(1),
                     Category=_categoryRepository.Single("Restaurant"),
                     Description="McDo St Jean"
                 }
             };
             _poiRepository.UpdateRange(pois);
             _poiRepository.Save();

             _logger.LogWarning("Added pois");*/
        }
    }
}