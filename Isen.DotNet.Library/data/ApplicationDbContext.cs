using Isen.DotNet.Library.Models.Implementation;
using Microsoft.EntityFrameworkCore;

namespace Isen.DotNet.Library.Data
{
    public class ApplicationDbContext : DbContext
    {
        // 1 - Préciser les DbSet
        public DbSet<Poi> PoiCollection { get; set; }
        public DbSet<Address> AddressCollection { get; set; }
        public DbSet<Town> TownCollection { get; set; }
        public DbSet<Category> CategoryCollection { get; set; }
        public DbSet<Departement> DepartementCollection { get; set; }

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options) 
            : base(options) { }

        protected override void OnModelCreating(
            ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            

            builder.Entity<Poi>()
                .ToTable("Poi");
            // .HasForeignKey(p => p.TownId);

            builder.Entity<Address>()
                .ToTable("Address");

            builder.Entity<Departement>()
                .ToTable("Departement");


            builder.Entity<Town>()
                .ToTable("Town");


            builder.Entity<Category>()
                .ToTable("Category");


        }
    }
}