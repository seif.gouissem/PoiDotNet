﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.DotNet.Library.Models.Base;

namespace Isen.DotNet.Library.Models.Implementation
{
    public class Town : BaseModel
    {
        public override string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Departement Departement { get; set; }
        public int? DepartementId { get; set; }



        public override string Display =>
            $"{base.Display}|Latitude={Latitude}|Longitude={Longitude}|Departement={Departement}";

        public override dynamic ToDynamic()
        {
            var response = base.ToDynamic();
            response.name = Name;
            response.latitude = Latitude;
            response.longitude = Longitude;
            response.departement = Departement?.ToDynamic();
            return response;
        }
    }
}
