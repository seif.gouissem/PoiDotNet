﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.DotNet.Library.Models.Base;

namespace Isen.DotNet.Library.Models.Implementation
{
    public class Address : BaseModel
    {
        public string TextLine { get; set; }
        public string ZipCode { get; set; }
        public Town Town { get; set; }
        public int? TownId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }



        public override string Display =>
            $"{base.Display}|TextLine={TextLine}|ZipCode={ZipCode}|Latitude={Latitude}|Longitude={Longitude}";

        public override dynamic ToDynamic()
        {
            var response = base.ToDynamic();
            response.name = TextLine;
            response.textline = TextLine;
            response.zipcode = ZipCode;
            response.town = Town?.ToDynamic();
            response.latitude = Latitude;
            response.longitude = Longitude;
            return response;
        }
    }
}
