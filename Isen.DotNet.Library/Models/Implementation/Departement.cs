﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.DotNet.Library.Models.Base;

namespace Isen.DotNet.Library.Models.Implementation
{
    public class Departement : BaseModel
    {
        public override string Name { get; set; }
        public string Code { get; set; }



        public override string Display =>
            $"{base.Display}|Code={Code}";

        public override dynamic ToDynamic()
        {
            var response = base.ToDynamic();
            response.name = Name;
            response.code = Code;
            return response;
        }
    }
}