﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.DotNet.Library.Models.Base;

namespace Isen.DotNet.Library.Models.Implementation
{
    public class Poi : BaseModel
    {
        public override string Name { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public int? CategoryId { get; set; }
        public Address Address { get; set; }
        public int? AddressId { get; set; }


        public override string Display =>
            $"{base.Display}|Description={Description}|Category={Category}|Adress={Address}";

        public override dynamic ToDynamic()
        {
            var response = base.ToDynamic();
            response.name = Name;
            response.description = Description;
            response.category = Category?.ToDynamic();
            response.address = Address?.ToDynamic();
            return response;
        }
    }
}