﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.DotNet.Library.Models.Base;

namespace Isen.DotNet.Library.Models.Implementation
{
    public class Category : BaseModel
    {
        public override string Name { get; set; }
        public string Description { get; set; }



        public override string Display =>
            $"{base.Display}|Description={Description}";

        public override dynamic ToDynamic()
        {
            var response = base.ToDynamic();
            response.name = Name;
            response.description = Description;
            return response;
        }
    }
}
